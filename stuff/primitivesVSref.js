let x= 7;

let obj = {a:1, s: "String"};
console.log(x);
console.log(obj);

function f(a,b){
    a = 15;
    // b = {a:20, s:"text"};
    b.s = "text";
    return;
}

f(x, obj);
console.log('---- after invoking the function f(a,b)');
console.log(x);
console.log(obj);