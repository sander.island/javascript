// Create a function called half which halves the value of a number and displays it to the console in an inner function.

function half(x) {
  x /= 2;
  function displayResult() {
    console.log(x);
  }
  displayResult();
}

half(12);
half(24);
half(352);
half(6);
half(26578);
half(56);
