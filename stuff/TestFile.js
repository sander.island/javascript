// FILE USED FOR TESTING RANDOM STUFF!

// Synchronous
console.log("Before the timeout");

// Event loop
setTimeout(function () {
  console.log("Inside the first timeout");
}, 0);

// Synchronous
console.log("After the timeout");

// Event loop
setTimeout(function () {
  console.log("Inside the second timeout");
}, 0);
