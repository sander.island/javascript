// Write a recursive function that calculates the factorial of a number

function factorial(number) {
  if (number == 0) {
    return 1;
  } else {
    return number * factorial(number - 1);
  }
}

console.log("Factorial of 3: " + factorial(3));
console.log("Factorial of 4: " + factorial(4));
console.log("Factorial of 5: " + factorial(5));
console.log("Factorial of 6: " + factorial(6));

// Fibonacci for fun
function fibonacci(number) {
  if (number < 2) {
    return number;
  } else {
    return fibonacci(number - 1) + fibonacci(number - 2);
  }
}

// Print Fibonacci
console.log("10 first numbers of Fibonacci:");
for (let i = 0; i < 10; i++) {
  console.log(fibonacci(i));
}
