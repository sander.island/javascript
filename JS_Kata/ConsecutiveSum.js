// bit_operations, logic, math, numbers, validation

// Create a function that takes a number n as an argument and checks whether the given number can be expressed as a sum of two or more consecutive positive numbers.
// Examples
// consecutiveSum(9) ➞ true
// // 9 can be expressed as a sum of (2 + 3 + 4) or (4 + 5).

// consecutiveSum(10) ➞ true
// // 10 can be expressed as a sum of 1 + 2 + 3 + 4.

// consecutiveSum(64) ➞ false

function consecutiveSum(number) {
  let result = false;
  if (number < 3) return false; // If the number is 2 or lower, it will not work

  for (let k = 1; k < number; k++) {
    let sum = 0;
    for (let i = k; i < number; i++) {
      sum += i;
      if (sum == number) return true;
    }
  }

  return result;
}

console.log(consecutiveSum(9));
console.log(consecutiveSum(10));
console.log(consecutiveSum(64));
console.log(consecutiveSum(15));
console.log(consecutiveSum(21));
