// Palindroms
// Given a string, return true if the string is a palindrome or false if it is not.
// Palindromes are strings that form the same word if it is reversed.
// Do include spaces and punctuation in determining if te string is a palindrome.
// Example:
// palindrome("abba") === true
// palindrome("abcdefg") ===false

// Check if a string is a palindrome using a for loop
function palindrome(str) {
  str = str.toLowerCase().replace(/[^A-Z0-9]+/gi, "");

  for (let i = 0; i < str.length / 2; i++) {
    if (str[i] !== str[str.length - 1 - i]) {
      return false;
    }
  }
  return true;
}

// Alternative solution
function palindrome1(str) {
  str = str.toLowerCase().replace(/[^A-Za-z0-9]/g, "");
  let reverseStr = str.split("").reverse().join("");
  return str === reverseStr;
}

console.log(palindrome("Hei jeg heter SAnder.!"));
console.log(palindrome("abba"));
console.log(palindrome("abcdefg"));

console.log(palindrome1("Hei jeg heter SAnder.!"));
console.log(palindrome1("abba"));
console.log(palindrome1("abcdefg"));
