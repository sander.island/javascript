// Given a 3X3 matrix of a completed tic-tac-toe game, create a function that returns
// whether the game is a win for "X" or "O", or a "Draw",
// where "X" and "O" represent themselves on the matrix, and "E" represent an empty spot.

// There is probably a better way to do this task, but this works.
// This solution checks every row, column and diagonal, and adds a "score" to keep track of who wins.

function ticTacToe([arr1, arr2, arr3]) {
    let x = 0;
    let o = 0;

    // CHECK FIRST COLUMN
    if (arr1[0] == "X" && arr2[0] == "X" && arr3[0] == "X") {
        x += 1;
    }
    else if (arr1[0] == "O" && arr2[0] == "O" && arr3[0] == "O") {
        o += 1;
    }

    // CHECK SECOND COLUMN
    if (arr1[1] == "X" && arr2[1] == "X" && arr3[1] == "X") {
        x += 1;
    }
    else if (arr1[1] == "O" && arr2[1] == "O" && arr3[1] == "O") {
        o += 1;
    }

    // CHECK THIRD COLUMN
    if (arr1[2] == "X" && arr2[2] == "X" && arr3[2] == "X") {
        x += 1;
    }
    else if (arr1[2] == "O" && arr2[2] == "O" && arr3[2] == "O") {
        o += 1;
    }

    // CHECK FIRST ROW
    if (arr1[0] == "X" && arr1[1] == "X" && arr1[2] == "X") {
        x += 1;
    }
    else if (arr1[0] == "O" && arr1[1] == "O" && arr1[2] == "O") {
        o += 1;
    }

    // CHECK SECOND ROW
    if (arr2[0] == "X" && arr2[1] == "X" && arr2[2] == "X") {
        x += 1;
    }
    else if (arr2[0] == "O" && arr2[1] == "O" && arr2[2] == "O") {
        o += 1;
    }

    // CHECK THIRD ROW
    if (arr3[0] == "X" && arr3[1] == "X" && arr3[2] == "X") {
        x += 1;
    }
    else if (arr3[0] == "O" && arr3[1] == "O" && arr3[2] == "O") {
        o += 1;
    }

    // CHECK DIAGONAL TOP LEFT TO RIGHT
    if (arr1[0] == "X" && arr2[1] == "X" && arr3[2] == "X") {
        x += 1;
    }
    else if (arr1[0] == "O" && arr2[1] == "O" && arr3[2] == "O") {
        o += 1;
    }

    // CHECK DIAGONAL TOP RIGHT TO LEFT
    if (arr1[2] == "X" && arr2[1] == "X" && arr3[0] == "X") {
        x += 1;
    }
    else if (arr1[2] == "O" && arr2[1] == "O" && arr3[0] == "O") {
        o += 1;
    }

    // CHECK WHO WINS
    if (x > o) {
        console.log("X wins");
    }
    else if (o > x) {
        console.log("O wins");
    }
    else console.log("Draw");
}


ticTacToe([
    ["X", "O", "X"],
    ["X", "O", "X"],
    ["X", "X", "O"]]); // X wins

ticTacToe([
    ["X", "O", "X"],
    ["O", "X", "O"],
    ["O", "X", "X"]]); // X wins

ticTacToe([
    ["O", "O", "O"],
    ["O", "X", "X"],
    ["E", "X", "X"]]); // O Wins

ticTacToe([
    ["X", "X", "X"],
    ["O", "O", ""],
    ["", "", ""]]); // X wins

ticTacToe([
    ["O", "O", "X"],
    ["X", "", "X"],
    ["O", "", "X"]]); // X wins

ticTacToe([
    ["X", "X", "O"],
    ["O", "O", "X"],
    ["X", "X", "O"]]); // Draw