// Freddy Fridays
// Write a program which counts and displays the number of occurrences of Friday
// the 13th in a particular year.
// For validation purposes, use the current year as the starting year and 3000 as the
// maximum year.

function friday13InYear(year) {
  if (year > 3000 || year < 2022) {
    console.log("Check fridays in year: " + year);
    console.log("The input year is outside the allowed interval!");
    console.log();
  } else {
    count = 0;
    console.log("Check fridays in year: " + year);
    const month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December",];
    for (let i = 0; i < 12; i++) {
      let date = new Date(year, i, 13);
      let dayOfWeek = date.getDay();

      if (dayOfWeek == 5) {
        count++;
        console.log("There is a Friday 13th in " + month[date.getMonth()]);
      }
    }
    console.log("Total Friday the 13's in " + year + ": " + count);
    console.log();
  }
}

friday13InYear(2021);
friday13InYear(2022);
friday13InYear(2023);
friday13InYear(2024);
friday13InYear(2025);
friday13InYear(2053);
