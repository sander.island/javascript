// Interview, language_fundamentals, loops, strings

// Create a function that takes two strings as arguments and returns the number of times the first string (the single character) is found in the second string.
// Examples
// charCount("a", "edabit") ➞ 1

// charCount("c", "Chamber of secrets") ➞ 1

// charCount("B", "boxes are fun") ➞ 0

// charCount("b", "big fat bubble") ➞ 4

// charCount("e", "javascript is good") ➞ 0

// charCount("!", "!easy!") ➞ 2
// Notes
// Your output must be case-sensitive (see second example).
// You can get the length of a string with the .length property

function charCount(letter, myString) {
  count = 0;
  /*
  Alternative loop
  for (var i = 0; i < myString.length; i++) {
    if (letter == myString[i]) {
      count++;
    }
  }
  */

  for (const s of myString) {
    if (s == letter) {
      count++;
    }
  }
  console.log(count);
}

charCount("a", "edabit");
charCount("c", "Chamber of secrets");
charCount("B", "boxes are fun");
charCount("b", "big fat bubble");
charCount("e", "javascript is good");
charCount("!", "!easy!");
