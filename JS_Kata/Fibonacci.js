// The Fibonacci Sequence is the series of numbers:
// 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, ...
// The next number is found by adding up the two numbers before it:
// the 2 is found by adding the two numbers before it (1+1),
// the 3 is found by adding the two numbers before it (1+2),
// the 5 is (2+3),
// and so on!
// Please generate x number in the sequence.
// Optional:
// Please generate only one number with x index

function fibonacci(number) {
  let fibonacci = [0, 1]; // Initialize Fibonacci array!

  for (let i = 2; i <= 100; i++) {
    fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1]; // Add the first 100 numbers of Fibonacci in the array
  }

  if (fibonacci.indexOf(number) > 2) {
    // A number in the Fibonacci sequence
    console.log(
      "The " +
        number +
        " is found by adding the two numbers before it (" +
        fibonacci[fibonacci.indexOf(number) - 2] +
        " + " +
        fibonacci[fibonacci.indexOf(number) - 1] +
        ")."
    );
  } else if (fibonacci.indexOf(number) === 1) {
    // There is two 1's in the Fibonacci sequence
    console.log(
      "The number 1 can either be one of the starting numbers or be the sum of 0 + 1."
    );
  } else if (fibonacci.indexOf(number) === 0) {
    // 0 is not the sum of any numbers
    console.log("The number is the starting number (0).");
  } else if (!isNaN(number)) {
    // The input number is not found in the Fibonacci sequence
    console.log("The number " + number + " is not in the Fibonacci sequence.");
  } else if (isNaN(number)) {
    // The input is not a number
    console.log('"' + number + '" is not a number.');
  }
}

// Found out the task actually was much simpler than I first interpreted...
// This is what I think it should have been hahah!
function fibonacci1(x) {
  let fibonacci = [0, 1]; // Initialize Fibonacci array!

  for (let i = 2; i <= x - 1; i++) {
    fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1]; // Add the first x numbers of Fibonacci in the array
  }
  console.log(fibonacci);
}

fibonacci(1);
fibonacci(3);
fibonacci(8);
fibonacci(12);
fibonacci(75);
fibonacci(144);
fibonacci(46368);
fibonacci(2178309);
fibonacci("hello");

fibonacci1(20);
