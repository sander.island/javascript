//Create a function that accepts a string as an argument.

// The function must move all capital letters to the front of a word, lowercase letters thereafter and lastly, numbers to the back. 
// It should return a string with the reordered word.

// Examples


// reorder("hA2p4Py") ➞ "APhpy24"


// reorder ("m11oveMENT") ➞ "MENTmove11"


// reorder ("s9hOrt4CAKE") ➞ "OCAKEshrt94"


// Notes
// Keep the original relative order of the upper and lower case letters the same as well as numbers

function reorder(word) {
    //
    var capitals = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var lowers = "abcdefghijklmnopqrstuvwxyz";
    var letter;
    var result = "";
    // Go through and add the capitals to the result string
    for (let i = 0; i < word.length; i++) {
        letter = word.charAt(i);
        if (capitals.indexOf(letter) != -1) {
            result = result + letter;
        }
    }
    // Go through and add the lowers to the result string
    for (let i = 0; i < word.length; i++) {
        letter = word.charAt(i);
        if (lowers.indexOf(letter) != -1) {
            result = result + letter;
        }
    }
    // Go through and add the numbers to the result string
    for (let i = 0; i < word.length; i++) {
        letter = word.charAt(i);
        if (!isNaN(letter)) {
            result = result + letter;
        }
    }
    return result;
}

// Alternative better way of doing the task
function reorderBetter(string) {
    let upper = string.replace(/[^A-Z]/g, "");
    let lower = string.replace(/[^a-z]/g, "");
    let number = string.replace(/[^1-9]/g, "");

    return upper + lower + number;
}

// Reorder
console.log(reorder("heLl34oOX"));
console.log(reorder("hA2p4Py"));
console.log(reorder("m11oveMENT"));
console.log(reorder("s9hOrt4CAKE"));

// Reorder Better
console.log(reorderBetter("heLl34oOX"));
console.log(reorderBetter("hA2p4Py"));
console.log(reorderBetter("m11oveMENT"));
console.log(reorderBetter("s9hOrt4CAKE"));