/*
Create a function that takes an array of numbers and returns the second largest number.
Examples
secondLargest([10, 40, 30, 20, 50]) ➞ 40
 
secondLargest([25, 143, 89, 13, 105]) ➞ 105
 
secondLargest([54, 23, 11, 17, 10]) ➞ 23
 
secondLargest([1, 1]) ➞ 0
 
secondLargest([1]) ➞ 1
 
secondLargest([]) ➞ 0
*/
 
//Notes
//If only one number exists, return that number

//When array only has two numbers that are equal, return 0

//Return 0 for an empty array

/*
const arr = [3, 5, 8, 100, 20];

const max = Math.max(...arr);

const index = arr.indexOf(max);
console.log(index);
*/

function secondLargest(input) {
    if(input.length == 0) return 0;
    if(input.length == 1) return input[0];
    if(input.length == 2 && input[0] == input[1]) return 0;
    input.sort((a, b) => b - a);    // Sorts the array to descending order
    return input[1];                // Returns the second number, which will be the second largest
}

console.log(secondLargest([10, 40, 30, 20, 50]));
console.log(secondLargest([25, 143, 89, 13, 105]));
console.log(secondLargest([54, 23, 11, 17, 10]));
console.log(secondLargest([1, 1]));
console.log(secondLargest([1]));
console.log(secondLargest([]));