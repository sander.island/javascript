// Write a function that takes a string and returns a string with the correct case for 
// character titles in the Game of Thrones series.
// - The words  and,  the,  of and  in should be lowercase.
// - All other words should have the first character as uppercase and the rest lowercase.
// - All commas must always be followed by a single space.
// - All titles must end with a period.
//
// Examples
// correctTitle("jOn SnoW, kINg IN thE noRth") ➞ "Jon Snow, King in the North."
// correctTitle("sansa stark,lady of winterfell.") ➞ "Sansa Stark, Lady of Winterfell."
// correctTitle("TYRION LANNISTER, HAND OF THE QUEEN.") ➞ "Tyrion Lannister, Hand of the Queen."

function correctTitle(str) {
    let sentence = str.toLowerCase(); // Make the whole sentence lowercase

    // Add space behind comma
    // Note: will not work with more than one comma...
    if(sentence[sentence.indexOf(",") + 1] != " ") {
        sentence = sentence.replaceAll(",", ", ");
    }
    
    // Add period at the end if there isn't
    if(sentence[sentence.length - 1] != ".") {
        sentence = sentence + ".";
    }

    let words = sentence.split(" "); // Create array of words

    let result = ""; // Result string

    // Iterate over each word and make every first character uppercase except "and"/"the"/"of"/"in"
    for (let i = 0; i < words.length; i++) {
        if (words[i] != "and" && words[i] != "the" && words[i] != "of" && words[i] != "in") {
            words[i] = words[i][0].toUpperCase() + words[i].substr(1);
            result += words[i] + " "; // Adding the words to result with space between them
        }
    }

    // Print result
    console.log(result);
}


correctTitle("jOn SnoW, kINg IN thE noRth");
correctTitle("sansa stark,lady of winterfell.");
correctTitle("TYRION LANNISTER, HAND OF THE QUEEN.");