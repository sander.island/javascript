// Max chars
// Given a string, return the character that is most commonly used in the string.
// Example:
// maxChar("abccccccd") === "c
// maxChar("apple 12311111") === "1"

function maxChar(str) {
  let strCounts = {};
  let maxChar = "";
  for (let i = 0; i < str.length; i++) {
    let key = str[i];
    if (!strCounts[key]) {
      strCounts[key] = 0;
    }
    strCounts[key]++;
    if (maxChar == "" || strCounts[key] > strCounts[maxChar]) {
      maxChar = key;
    }
  }
  console.log(str + ": " + maxChar);
}

maxChar("abccccccd");
maxChar("apple 12311111");